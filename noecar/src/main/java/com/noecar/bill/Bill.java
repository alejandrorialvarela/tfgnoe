package com.noecar.bill;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.noecar.appointment.Appointment;
import com.noecar.car.Car;

@Entity
@Table(name = "bill")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "billId")
public class Bill implements java.io.Serializable{
	
	private static final long serialVersionUID = 8375700752307488451L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "billId")
	private Long billId;
	
	private String description;
		
	private Date fecha;
	
	private Integer quantity;
	
	private BigInteger importe;
	
	private BigInteger cif;
	
	private Integer iva;
	
	private Integer total;
	
	@OneToOne
	@JoinColumn(name = "appointmentId")
	private Appointment appointment;

	

	public Bill(Long billId, String description, Date fecha, Integer quantity, BigInteger importe, BigInteger cif,
			Integer iva, Integer total, Appointment appointment) {
		super();
		this.billId = billId;
		this.description = description;
		this.fecha = fecha;
		this.quantity = quantity;
		this.importe = importe;
		this.cif = cif;
		this.iva = iva;
		this.total = total;
		this.appointment = appointment;
	}

	public Bill() {
		super();
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public BigInteger getImporte() {
		return importe;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImporte(BigInteger importe) {
		this.importe = importe;
	}

	public BigInteger getCif() {
		return cif;
	}

	public void setCif(BigInteger cif) {
		this.cif = cif;
	}
	

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getIva() {
		return iva;
	}

	public void setIva(Integer iva) {
		this.iva = iva;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}
	
	

	


}
