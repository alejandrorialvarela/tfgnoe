package com.noecar.bill;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.noecar.account.AccountRepository;
import com.noecar.appointment.Appointment;
import com.noecar.appointment.AppointmentRepository;
import com.noecar.car.Car;
import com.noecar.car.CarRepository;

@Service
@Transactional
public class BillServiceImpl implements BillService {

	
	@Autowired
	BillRepository billRepository;

	@Autowired
	CarRepository carRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	AppointmentRepository appointmentRepository;

	@Override
	public Boolean create(Bill bill, Long entityId) throws OperationsException {
		// Account account = accountRepository.findOneByEmail(email);
		Appointment appointment = appointmentRepository.findOne(entityId);
		if (bill != null && appointment != null) {
			bill.setAppointment(appointment);
			billRepository.saveAndFlush(bill);
			return Boolean.TRUE;
		}
		else {
			return Boolean.FALSE;
		}
	}

	@Transactional
	public Boolean delete(Long billId) throws OperationsException {
		Bill bill = billRepository.findOne(billId);
		if (bill != null) {
			billRepository.delete(bill);
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	@Override
	public List<Bill> findAll() {
		return billRepository.findAll();
	}

}
