package com.noecar.bill;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class BillController {
	
	@Autowired
	BillService billService;
	
	
	@PostMapping("bill/{entityId}")
	@ResponseBody
	public Boolean create(@RequestBody Bill bill, @PathVariable("entityId") Long entityId ) throws OperationsException {
		return billService.create(bill, entityId);
	}
	
	@GetMapping("bill")
	@ResponseBody
	public List<Bill> findAll() {
		return billService.findAll();
	}
	
	@DeleteMapping("bill/{billId}")
	@ResponseBody
	public Boolean delete(@PathVariable("billId") Long billId ) throws OperationsException {
		return billService.delete(billId);
	}
	

}
