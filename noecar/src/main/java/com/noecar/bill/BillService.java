package com.noecar.bill;

import java.util.List;
import javax.management.OperationsException;

public interface BillService {

	public Boolean create(Bill bill,Long appointmentId) throws OperationsException;
	
	public Boolean delete(Long billId) throws OperationsException;
	
	public List<Bill> findAll();

}
