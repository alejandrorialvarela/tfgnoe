package com.noecar.appointment;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.noecar.account.Account;
import com.noecar.bill.Bill;
import com.noecar.car.Car;

@Entity
@Table(name = "appointment")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "appointmentId")
public class Appointment implements java.io.Serializable {

	private static final long serialVersionUID = 1055738413627464397L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "appointmentId")
	private Long appointmentId;

	private String date;
	
	@ManyToOne
	@JoinColumn(name = "carId")
	@JsonBackReference(value="car - appointment")
	private Car car;
	

	private String descripcion;

	private String hours;

	private String tipo; //reparación o mantenimiento
	
	private String state; 

	@OneToOne(mappedBy = "appointment", fetch = FetchType.EAGER)
	private Bill bill;

	public Appointment(Long appointmentId, String date, String descripcion,
			String hours, String tipo) {
		super();
		this.appointmentId = appointmentId;
		this.date = date;
		this.setDescripcion(descripcion);
		this.setHours(hours);
		this.setTipo(tipo);
		
	}
	
	public Appointment(Long appointmentId, String date, String descripcion,
			String hours, String tipo, Bill bill) {
		super();
		this.appointmentId = appointmentId;
		this.date = date;
		this.setDescripcion(descripcion);
		this.setHours(hours);
		this.setTipo(tipo);
		this.bill=bill;
	}


	public Appointment() {
		super();
	}


	public Long getAppointmentId() {
		return appointmentId;
	}


	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	
	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public Car getCar() {
		return car;
	}


	public void setCar(Car car) {
		this.car = car;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
