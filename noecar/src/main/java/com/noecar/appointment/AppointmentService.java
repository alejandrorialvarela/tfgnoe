package com.noecar.appointment;

import java.util.List;
import javax.management.OperationsException;

import com.noecar.car.Car;

public interface AppointmentService {
	
	public Boolean create(Appointment appointment, Long carId, String email) throws OperationsException;

	public Boolean delete(Long appointmentId) throws OperationsException;

	public List<Appointment> findAll();

	public Boolean update(Long appointmentId, String state) throws OperationsException;


}
