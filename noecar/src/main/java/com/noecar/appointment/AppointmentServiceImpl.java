package com.noecar.appointment;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.noecar.account.Account;
import com.noecar.account.AccountRepository;
import com.noecar.bill.BillRepository;
import com.noecar.car.Car;
import com.noecar.car.CarRepository;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	AppointmentRepository appointmentRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	BillRepository billRepository;

	@Autowired
	CarRepository carRepository;

	// Se le pasa el email para indiappointment el usuario al que pertenece el
	// appointment
	@Override
	public Boolean create(Appointment appointment, Long carId, String email) throws OperationsException {
		Account account = accountRepository.findOneByEmail(email);
		Car car = carRepository.findOne(carId);
		if (appointment != null && account != null && car != null) {
			appointment.setCar(car);
			appointmentRepository.saveAndFlush(appointment);
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	@Override
	public Boolean update(Long appointmentId, String state ) throws OperationsException {
		Appointment appointment = appointmentRepository.findOne(appointmentId);
			appointment.setState(state);
			appointmentRepository.saveAndFlush(appointment);
			return Boolean.TRUE;
	}
	

	@Transactional
	public Boolean delete(Long appointmentId) throws OperationsException {
		Appointment appointment = appointmentRepository.findOne(appointmentId);
		if (appointment != null) {
			if (appointment.getBill() != null) {
					billRepository.delete(appointment.getBill());
				}
				appointmentRepository.delete(appointment);
			return Boolean.TRUE;
		} else
			return Boolean.FALSE;
	}

	@Override
	public List<Appointment> findAll() {
		return appointmentRepository.findAll();
	}


}
