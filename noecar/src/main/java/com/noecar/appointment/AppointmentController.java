package com.noecar.appointment;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AppointmentController{
	
	@Autowired
	AppointmentService appointmentService;
	
	//Hai que pasar el email en el path para indiappointment de quién es el appointment
	@CrossOrigin
	@PostMapping("appointmentCreate/{carId}/{email}")
	@ResponseBody
	public Boolean create(@RequestBody Appointment appointment, @PathVariable("carId") Long carId, @PathVariable("email") String email ) throws OperationsException {
		return appointmentService.create(appointment, carId, email);
	}
	
	@CrossOrigin
	@PostMapping("update/{appointmentId}/{state}")
	@ResponseBody
	public Boolean update(@PathVariable("appointmentId") Long appointmentId, @PathVariable("state") String state ) throws OperationsException {
		return appointmentService.update(appointmentId, state);
	}
	
	@CrossOrigin
	@GetMapping("appointment")
	@ResponseBody
	public List<Appointment> findAll() {
		return appointmentService.findAll();
	}
	
	
	@CrossOrigin
	@DeleteMapping("deleteAppointment/{appointmentId}")
	@ResponseBody
	public Boolean delete(@PathVariable("appointmentId") Long appointmentId) throws OperationsException {
		return appointmentService.delete(appointmentId);
	}
	
	
	
	

}
