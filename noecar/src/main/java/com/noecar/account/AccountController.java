package com.noecar.account;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.noecar.account.AccountService;
import com.noecar.appointment.Appointment;
import com.noecar.car.Car;
@Controller
public class AccountController {
	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	AccountService accountService;
	
	@GetMapping("account/{email}")
	@ResponseBody
	public Account getUser(@PathVariable("email") String email) {
		return accountService.getUser(email);
	}
	
	@GetMapping("carsAccount/{email}")
	@ResponseBody
	public List<Car> getCarsUser(@PathVariable("email") String email) {
		return accountService.getCarsUser(email);
	}
	
	@PostMapping("signup")
	@ResponseBody
	public Boolean signup(@RequestBody Account account) throws OperationsException {
		Boolean a = accountService.create(account);
		return a;
	}
	
	@PostMapping("login")
	@ResponseBody
	public Boolean login(@RequestBody Account account) throws OperationsException {
		Boolean a = accountService.login(account);
		return a;
	}
	
	@GetMapping("users")
	@ResponseBody
	public List<Account> findAll() {
		return accountService.findAll();
	}
	
	@DeleteMapping("signoff/{email}")
	@ResponseBody
	public Boolean signoff(@PathVariable("email") String email) {
		accountService.delete(email);
		return Boolean.TRUE;
	}

}
