package com.noecar.account;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.Transactional;

import com.noecar.account.Account;
import com.noecar.appointment.Appointment;
import com.noecar.appointment.AppointmentRepository;
import com.noecar.bill.BillRepository;
import com.noecar.car.Car;
import com.noecar.car.CarRepository;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService implements UserDetailsService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private CarRepository carRepository;

	@Autowired
	private BillRepository billRepository;
	
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	// Add an user to the Data Base
	@Transactional
	public Boolean create(Account account) {
		Account a = accountRepository.findOneByEmail(account.getEmail());
		if (a == null) {
			accountRepository.save(account);
			return Boolean.TRUE;
		} else
			return Boolean.FALSE;
	}

	@Transactional
	public Boolean login(Account account) {
		Account a = accountRepository.findOneByEmail(account.getEmail());
		if (a != null && account.getPassword().equals(a.getPassword())) {
			return Boolean.TRUE;
		} else
			return Boolean.FALSE;
	}

	@Transactional
	public Boolean delete(String email) {
		Account account = accountRepository.findOneByEmail(email);
		List <Car> carList = account.getCarList();
		
		if (account != null) {
		for (int a=0; a< carList.size();a++) {
			Car car =carList.get(a);
				if (car != null) {
					List<Appointment> appointmentList = car.getAppointmentList(); //cojo la lista de citas que tengo que eliminar antes de eliminar el coche
					if (appointmentList.size() != 0) {
						for (int i = 0; i < appointmentList.size(); i++) {
							Appointment appointment = appointmentList.get(i);
							if (appointment != null) { //tengo que eliminar las repairs y mantenimientos antes de eliminar el appointment
								if(appointment.getBill()!=null) {
									billRepository.delete(appointment.getBill());
								}
								appointmentRepository.delete(appointment); 
							} else if (appointment != null) { //tengo que eliminar las repairs antes de eliminar el appointment
								if(appointment.getBill()!=null) {
									billRepository.delete(appointment.getBill());
								} 
								appointmentRepository.delete(appointment);
							} else {
								appointmentRepository.delete(appointment);
							}
						}
					}
					carRepository.delete(car); //tenga o no appointments, el ultimo paso siempre es eliminar el coche
					return Boolean.TRUE;
				}
			}	
			accountRepository.delete(account);
			return Boolean.TRUE;
		}else return Boolean.FALSE;
}

	@Transactional
	public Account getUser(String email) {
		Account a = accountRepository.findOneByEmail(email);
		if (a != null) {
			return a;
		} else
			return null;
	}
	
	@Transactional
	public List<Car> getCarsUser(String email) {
		Account a = accountRepository.findOneByEmail(email);
		List<Car> coches = a.getCarList();
		if (a != null && coches!=null) {
			return coches;
		} else
			return null;
	}


	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}


	public List<Account> findAll() {
		return accountRepository.findAll();
	}

}
