package com.noecar.account;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.noecar.car.Car;

@Entity
@Table(name = "account")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "accountId")
public class Account implements java.io.Serializable {
	
	private static final long serialVersionUID = 8375700752307488451L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "accountId")
	private Long accountId;

	private String name;

	private String email;
	
	private String password;
	
	//Relación con la tabla de car - Un account puede tener varios cars
	@OneToMany(mappedBy="account",fetch = FetchType.EAGER)
	@JsonManagedReference (value="account - car")
	private List<Car> carList;

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	//Constructor para crear un account
	public Account(Long accountId, String name, String email, String password) {
		super();
		this.accountId = accountId;
		this.name = name;
		this.email = email;
		this.password = password;
	}

	//Constructor para crear un account con la lista de car incluida
	public Account(Long accountId, String name, String email, String password, List<Car> carList) {
		super();
		this.accountId = accountId;
		this.name = name;
		this.email = email;
		this.password = password;
		this.carList = carList;
	}



	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public List<Car> getCarList() {
		return carList;
	}

	public void setCarList(List<Car> carList) {
		this.carList = carList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

}
