package com.noecar.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
		.allowedOrigins(" http://192.168.232.1:8103", "http://localhost:8103","http://localhost:8100","http://localhost:8101","http://192.168.1.105:8103", "http://192.168.195.1:8103", "http://192.168.1.105:8101", "http://192.168.195.1:8101" ) 
	    .allowedMethods("PUT", "DELETE", "POST", "GET"); 
	}
}