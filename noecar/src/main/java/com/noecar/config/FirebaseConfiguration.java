package com.noecar.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
@ComponentScan
public class FirebaseConfiguration {
	@Autowired
	private ResourceLoader resourceLoader;

	@Bean
	public FirebaseApp firebaseBuyerApp() {
		
		FileInputStream serviceAccount;
		FirebaseOptions options;
		try {
			serviceAccount = new FileInputStream("src/main/resources/travelapp-1994-firebase-adminsdk-8ge5v-1da83eaf25.json");
			options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://travelapp-1994.firebaseio.com")
			  .build();
			FirebaseApp.initializeApp(options);
		} catch (IOException e) {
			return null;
		}
		return null;

		
	}

}