package com.noecar.car;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.noecar.account.Account;
import com.noecar.account.AccountRepository;
import com.noecar.appointment.Appointment;
import com.noecar.appointment.AppointmentRepository;
import com.noecar.bill.Bill;
import com.noecar.bill.BillRepository;

@Service
@Transactional
public class CarServiceImpl implements CarService {

	@Autowired
	CarRepository carRepository;

	@Autowired
	AppointmentRepository appointmentRepository;
	
	@Autowired
	BillRepository billRepository;

	@Autowired
	AccountRepository accountRepository;

	// Se le pasa el email para indicar el usuario al que pertenece el car
	@Override
	public Boolean create(Car car, String email) throws OperationsException {
		Account account = accountRepository.findOneByEmail(email);
		if (car != null && account != null) {
			car.setAccount(account);
			carRepository.saveAndFlush(car);
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	@Override
	public List<Car> findAll() {
		return carRepository.findAll();
	}
	
	@Override
	public Boolean delete(Long carId) {
		Car car = carRepository.findOne(carId);
		if (car != null) {
			List<Appointment> appointmentList = car.getAppointmentList(); //cojo la lista de citas que tengo que eliminar antes de eliminar el coche
			if (appointmentList.size() != 0) {
				for (int i = 0; i < appointmentList.size(); i++) {
					Appointment appointment = appointmentList.get(i);
					if (appointment != null) { //tengo que eliminar las repairs y mantenimientos antes de eliminar el appointment
						if(appointment.getBill()!=null) {
							billRepository.delete(appointment.getBill());
						}
						appointmentRepository.delete(appointment); 
					} else if (appointment != null) { //tengo que eliminar las repairs antes de eliminar el appointment
						if(appointment.getBill()!=null) {
							billRepository.delete(appointment.getBill());
						} 
						appointmentRepository.delete(appointment);
					} else {
						appointmentRepository.delete(appointment);
					}
				}
			}
			carRepository.delete(car); //tenga o no appointments, el ultimo paso siempre es eliminar el coche
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	

}
