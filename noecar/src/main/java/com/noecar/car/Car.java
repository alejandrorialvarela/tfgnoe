package com.noecar.car;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.noecar.account.Account;
import com.noecar.appointment.Appointment;

@Entity
@Table(name = "car")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "carId")
public class Car implements java.io.Serializable {

	private static final long serialVersionUID = 1055738413627464397L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "carId")
	private Long carId;

	private String name;

	private String cost;
	
	private Long ano;
	
	private String combustible;
	
	private String color;
	
	private String potencia;
	
	//Realacion con la tabla Account - Un account puede tener varios car
	@ManyToOne
	@JoinColumn(name = "accountId")
	@JsonBackReference(value="account - car")
	private Account account;
	
	@OneToMany(mappedBy="car", fetch = FetchType.EAGER)
	@JsonManagedReference (value= "car - appointment")
	private List<Appointment> appointmentList;
	
	
	//Constructor para crear el car sin Account
	public Car(Long carId, String name, String cost,Long ano, String combustible, String color, String potencia) {
		super();
		this.carId = carId;
		this.name = name;
		this.cost = cost;
		this.ano=ano;
		this.combustible=combustible;
		this.color=color;
		this.potencia=potencia;
	}

	//Constructor para crear el car con Account
	public Car(Long carId, String name, String cost,Long ano, String combustible, String color, String potencia, Account account) {
		super();
		this.carId = carId;
		this.name = name;
		this.cost = cost;
		this.ano=ano;
		this.combustible=combustible;
		this.color=color;
		this.potencia=potencia;
		this.account = account;
	}



	public Car(Long carId, String name, String cost, Long ano, String combustible, String color, String potencia,Account account, List<Appointment> appointmentList) {
		super();
		this.carId = carId;
		this.name = name;
		this.cost = cost;
		this.ano=ano;
		this.combustible=combustible;
		this.color=color;
		this.potencia=potencia;
		this.account = account;
		this.appointmentList = appointmentList;
	}


	public Car() {
		super();
	}


	public Long getCarId() {
		return carId;
	}


	public void setCarId(Long carId) {
		this.carId = carId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}
	
	public Long getAno() {
		return ano;
	}

	public void setAno(Long ano) {
		this.ano = ano;
	}

	public String getCombustible() {
		return combustible;
	}

	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPotencia() {
		return potencia;
	}

	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}
	

	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}

	public List<Appointment> getAppointmentList() {
		return appointmentList;
	}

	public void setAppointmentList(List<Appointment> appointmentList) {
		this.appointmentList = appointmentList;
	}

}
	
