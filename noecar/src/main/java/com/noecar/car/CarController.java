package com.noecar.car;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.noecar.appointment.Appointment;

@Controller
public class CarController{
	
	@Autowired
	CarService carService;
	
	
	//Hai que pasar el email en el path para indicar de quién es el car
	@PostMapping("car/{email}")
	@ResponseBody
	public Boolean create(@RequestBody Car car, @PathVariable("email") String email ) throws OperationsException {
		return carService.create(car, email);
	}
	
	@DeleteMapping("deleteCar/{carId}")
	@ResponseBody
	public Boolean delete(@PathVariable("carId") Long carId ) throws OperationsException {
		return carService.delete(carId);
	}
	
	@GetMapping("car")
	@ResponseBody
	public List<Car> findAll() {
		return carService.findAll();
	}
	

}
