package com.noecar.car;

import java.util.List;
import javax.management.OperationsException;

import com.noecar.appointment.Appointment;

public interface CarService {
	
	public Boolean create(Car car, String email) throws OperationsException;
	
	public Boolean delete(Long carId);
	
	public List<Car> findAll();



}
